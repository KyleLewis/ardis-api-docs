.. Ardis API documentation master file, created by
   sphinx-quickstart on Mon Feb 10 10:21:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation for Ardis AI's API!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

* :ref:`search`

The endpoints described below allow users to create and run labeling jobs from 
indexed batches of documents, check the status of those jobs, retrieve indexed 
results, and delete jobs that are not actively being run. The default behavior 
of the service is to pull newly submitted jobs off of a queue, so after calling 
/create, your job will be run as soon as a worker is ready to accept it.

There is not yet an endpoint to stop a running job. If you mistakenly start a
large job and fail to delete it before it is run, get in touch with us, and we
can remove the job manually so that it doesn't block other jobs you may want to
start.

The main service is currently running on a single C4.xlarge AWS instance. With
those resources, you will likely be able to process documents of ~250 words 
at a rate of ~100-150 documents per hour.

Methods
==================

* `/job/create`_

* `/job/status`_

* `/job/results`_

* `/job/delete`_

* `/jobs`_


/job/create
~~~~~~~~~~~~~~~

:methods:
- PUT


:params:
- reports (dict) (Required): A dictionary holding the reports that will
  be processed as a part of the job that is created. It should be passed 
  as request data. The dictionary should be in the format shown below. Note
  that "index" and "report" are required keys. The index you provide with a 
  report will be used to link results to the correct report. Indices must be
  unique within a job.


:returns:
- job_id (int): The ID of the job that was created, which can be used to 
  check on its status later. You can recover all of your job IDs from the
  /jobs endpoint as well.

Example call (Python): 

.. code-block:: python

    reports = [{"index": 0, "report": "Report text here."},
               {"index": 1, "report": "More report text here."},
               {"index": 2, "report": "Even more report text here."}]
    url = "http://ardis-url:8009/job/create"
    auth = ("AuthName", "AuthKey")
    data = {"reports": json.dumps(reports)}
    result = requests.put(url, data=data, auth=auth)

Result:

.. code-block:: json

    { "job_id": 218 }

/job/status
~~~~~~~~~~~~~~~

:methods:
- GET

:params:
- job_id (int) (Required): The job_id of the job you want the status for.

Returns the status of the job, including how many documents it contains,
how many have been completed, and the overall status of the job, e.g.,
"UNASSIGNED", "WORKING", "SUCCEEDED", "FAILED."

Example call (Python):

.. code-block:: python

  url = "http://ardis-url:8009/job/status"
  auth = ("AuthName", "AuthKey")
  data = {"job_id": 218}
  result = requests.get(url, data=data, auth=auth)

Result:

.. code-block:: json

    {
      "job_id": 218,
      "job_size": 500,
      "job_status": "WORKING",
      "num_completed": 32
    }

/job/results
~~~~~~~~~~~~~~~

:methods:
- GET

:params:
- job_id (int) (Required): The job_id of the job you want the results for.

Returns the results of the job. This endpoint can be called while the job is 
still "WORKING" to retrieve partial results. For a job with the status
"SUCCEEDED" or "FAILED", the endpoint will return results for all documents in
the job. The result for each document in the job will include a "labels" field, 
which is a dictionary containing the labels applied to the document. If the
document was not sucessfully processed, its "labels" field will be an empty
dictionary and its "status" will be "FAILLED."

Example call (Python):

.. code-block:: python

  url = "http://ardis-url:8009/job/results"
  auth = ("AuthName", "AuthKey")
  data = {"job_id": 218}
  result = requests.get(url, data=data, auth=auth)

Result:

.. code-block:: json

  {
    "job_id": 218,
    "results": [
      {"index": 0,
       "report": "Report text here.",
       "status": "SUCCEEDED",
       "labels": {
         "label_a": "value_a",
         "label_b": "value_b",}
      },
      {"index": 1,
       "report": "More report text here.",
       "status": "WORKING",
       "labels": {},
      }
    ]
  }

/job/delete
~~~~~~~~~~~~~~~

:methods:
- PUT

:params:
- job_id (int) (Required): The job_id of the job you want to delete.

Deletes the job and all of its associated documents from the database. 
You won't be able to retrieve the jobs results after this call. Calling 
delete on a job that is status "RUNNING" will do nothing and return a 403.

Example call (Python):

.. code-block:: python

  url = "http://ardis-url:8009/job/delete"
  auth = ("AuthName", "AuthKey")
  data = {"job_id": 218}
  result = requests.put(url, data=data, auth=auth)

/jobs
~~~~~~~~~~~~~~~~

:methods:
- GET

Gets all of the jobs owned by the caller. They are returned in a list of 
dictionaries in the same format as the /job/status endpoint. This is a useful
way to retrieve your job IDs if you didn't catch them on job creation.

Example call (Python):

.. code-block:: python

  url = "http://ardis-url:8009/jobs"
  auth = ("AuthName", "AuthKey")
  result = requests.get(url, auth=auth)

Result:

.. code-block:: json

    [{"job_id": 218,
      "job_size": 500,
      "job_status": "WORKING",
      "num_completed": 32
     },{
      "job_id": 219,
      "job_size": 200,
      "job_status": "UNASSIGNED",
      "num_completed": 0
     }